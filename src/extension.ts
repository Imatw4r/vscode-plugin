// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import * as vsls from 'vsls/vscode';
import * as rp from 'request-promise';

export function activate(context: vscode.ExtensionContext) {
	// TODO: ext needs to support Microsoft auth
	// need to open website asking to login as Live Share does
	// or not if user is already authenticated

	let currentTime = vscode.commands.registerCommand('extension.share', async () => {
		const liveshare = await getVslsApi();
		if (liveshare === null) {
			return;
		}

		let session = await liveshare.share();
		if (session === null) {
			return;
		}
		let sessionUrl: URL = session.toString();
		const { machineId } = vscode.env;
		const shareableUrl = await register(machineId, sessionUrl);
		if (shareableUrl) {
			vscode.window
				.showInformationMessage(`This is your URL ${shareableUrl}`, ...["Copy to clipboard."])
				.then(() => {
					vscode.env.clipboard.writeText(shareableUrl);
				});
		}
	});
	context.subscriptions.push(currentTime);
}

// this method is called when your extension is deactivated
export function deactivate() { }

async function getVslsApi(): Promise<vsls.LiveShare | null> {
	const liveshare = await vsls.getApi();
	return liveshare;
}

type MachineID = string;
type URL = string;

const register = async function registerMachineIDAndSessionUrl(
	machineId: MachineID, sessionUrl: URL): Promise<URL | null> {
		// TODO: handle failures here
	let shareableUrl = await rp.post(
		{
			url: "https://oversharing.azurewebsites.net/api/register",
			json: {
				"machineId": machineId,
				"sessionUrl": sessionUrl
			}, 
			followRedirect: false
		}).catch(er => {
			vscode.window.showErrorMessage("Unable to link your session now.");
			// FIXME: worth checking if in such case next call would
			// only see if sessions is open and reuse it
			return null;
		});

	return shareableUrl;
};