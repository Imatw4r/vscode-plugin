# eyescream README

Make sharing live session much easier by generating persistent link instead of randomly generated one.

## Features

Write here about plugin https://github.com/MicrosoftDocs/live-share/issues/708
Might get some users

## Requirements

`vsls` library.


## Release Notes

Users appreciate release notes as you update your extension.

### 1.0.0

Initial release of vscode plugin


**Enjoy!**